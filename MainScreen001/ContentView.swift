//
//  ContentView.swift
//  MainScreen001
//
//  Created by Leadconsultant on 11/3/19.
//  Copyright © 2019 Leadconsultant. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    

    @State var textFieldData = " "
    var body: some View {
        TabView{
            
            //FirstScreen
        ZStack{
            Rectangle().foregroundColor(.white).opacity(0.4).edgesIgnoringSafeArea(.top) //Background Rectangle
            VStack{
                
                ZStack(alignment: .top){
                    Rectangle().foregroundColor(.red).cornerRadius(100, antialiased: true, corners: [.bottomLeft]).frame( height: 200)
                ZStack{
                Circle()
                    .fill(Color.white)
                    .padding(123)
                    Circle()
                        .fill(Color.red)
                        .padding(130)
                    }
                }.edgesIgnoringSafeArea(.all)
                

                
                ZStack{
                    Rectangle().foregroundColor(.clear)
                    Image("CardImage").resizable().frame(width: 350, height: 200, alignment: .top).offset(CGSize(width: 0, height: -200))
                }.edgesIgnoringSafeArea(.all)
                
                
            }
            
            }.tabItem {
                Text("Tab 1").foregroundColor(.red)
                
            }
            
           //Second Screen
            Text("This is second TabView").tabItem {
                Text("Tab 2")
            }
            
        }.edgesIgnoringSafeArea(.top).accentColor(.red)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

extension View {
    func cornerRadius(_ radius: CGFloat, antialiased: Bool = true, corners: UIRectCorner) -> some View {
        clipShape(
            RoundedCorner(radius: radius, style: antialiased ? .continuous : .circular, corners: corners)
        )
    }
}

struct RoundedCorner: Shape {
    
    var radius: CGFloat = .infinity
    var style: RoundedCornerStyle = .continuous
    var corners: UIRectCorner = .allCorners
    
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}
